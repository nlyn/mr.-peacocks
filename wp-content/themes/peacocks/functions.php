<?php

function remove_menus()
{
    global $menu;
    global $current_user;
    get_currentuserinfo();
    if($current_user->user_login == 'rox')
    {
        $restricted = array(__('Posts'),
                            __('Media'),
                            __('Links'),
                            __('Comments'),
                            __('Appearance'),
                            __('Plugins'),
                            __('Users'),
                            __('Tools'),
                            __('Settings')
        );
        end ($menu);
        while (prev($menu)){
            $value = explode(' ',$menu[key($menu)][0]);
            if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
        }// end while
    }// end if
}
add_action('admin_menu', 'remove_menus');



function add_markup_pages($output) {
    $output= preg_replace('/page-item/', ' first-page-item page-item', $output, 1);
	$output=substr_replace($output, " last-page-item page-item", strripos($output, "page-item"), strlen("page-item"));
    return $output;
}
add_filter('wp_list_pages', 'add_markup_pages');


// THIS THEME USES wp_nav_menu() IN TWO LOCATIONS FOR CUSTOM MENU.
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
		  'primary' => 'Primary Header Nav',
		)
	);
}

?>